+++

+++
## Die Raederkur

\- belebende Heilbehandlung für erschöpfte Patienten -

So bezeichnet man eine Kurbehandlung, die nach einem individuell erstellten Heilplan durchgeführt wird. Dem Patienten "Rad" werden hierbei verschiedene medizinische Heilbehandlungen verschrieben, wobei er in der Regel unter fachmännischer Kontrolle steht. Die angewandten medizinischen Maßnahmen variieren je nach Verschleiß und Behandlungsart. Man unterscheidet zwischen leichten bis tiefgreifenden Maßnahmen, zwischen Schönheits-, Gesundheits- oder Kur-"bädern". Je nach Ansatz werden dem Rad verschiedene Zusätze beigefügt, die unterschiedliche Auswirkungen haben.

Früher gab es sehr viele **Raederkuren** und jeder Kurort hatte diesbezüglich einige Pflege-Stationen, die mit großer Selbstverständlichkeit genutzt wurden. Fast alle Patienten wurden behandelt und kuriert und nicht, wie heutzutage, einfach seelenlos ausgetauscht. Durch einen wieder auflebenden, ressourcenschonenden und werteorientierten neuen Blick auf den Patienten "Rad" erleben **Raederkuren** hier einen ganz neuen Aufschwung. Viele Besitzer von alten Rädern sind auf die Wellness-Schiene umgestiegen und wünschen sich, den wunderbaren Charme ihrer zum Teil nostalgischen Raritäten einer belebenden Kur zu unterziehen, oder ihm im Extremfall sogar ein komplett neues Leben einzuhauchen.

**Die Raederkur** – Spaß an Bewährtem und Neuerfahrungen