---
date: 2019-07-24T08:03:29.000+00:00
title: RAEDERKUR - Projekt 04 - Umbau altes Rennrad „Koga Miyata“ - August 2017
subtitle: Rennrad - Single Speed (2-Gang-Automatik-Schaltung)
tags: []
previewimage: "/uploads/Pro4_01.jpg"

---
**Das Ausgangsmaterial:**

Ein sehr schönes, gut erhaltenes Koga Miyata-Rennrad, ca. Mitte 80er-Jahre, soll zu einem schicken, unkomplizierten Rad für die „sportlich-ambitionierte Fahrt zur Eisdiele“ umgebaut werden. Die Grundsubstanz von Rahmen, Tretlager und Steuersatz ist uneingeschränkt als exzellent zu bezeichnen. Der filigrane Charakter des alten Stahlrahmens soll mit den neuen Bauteilen, farblich und optisch, eine neue, in sich geschlossen wirkende Einheit ergeben. Hierzu wählt der Besitzer ein glänzendes, edles, leichtes Grau für den Rahmen aus, kombiniert mit weißen und schwarzen Akzenten für die neuen Anbauteile.

**Die Arbeitsschritte:**

Das Rad zerlegen und die weiterverwendbaren Teile aufarbeiten. Die Tretkurbel wird dabei auf ein 42er Kettenblatt reduziert. Schleifen, Anlaugen, Abkleben, Grundieren. Anschließend kommt der helle Grauton für den Rahmen ins Spiel. Ein gutes und edles Ergebnis. Keine Läufer, super :)

**Die Neuteile/Anbauteile:**

Der neue Vorbau und der MTB-Lenker sind beide weiß lackiert, ebenfalls die neue Sattelstütze (sie war übrigens in passender Farbe – weiß – und benötigtem Umfang so nicht zu bekommen, somit muss ich hier noch eine Reduzierhülse verarbeiten – das funktioniert aber einwandfrei). Dann die neuen Felgenbremsen von MICHE in Schwarz mit AVID-Bremsgriffen einbauen.

![](/uploads/Miche-Performance.jpg)

Schwarze Lenkergriffe mit weißen Applikationen. Die neuen Laufräder mit allroundtauglicher 32er-Bereifung und schwarzen Felgen sehen super aus. Ausgehend von der weiteren Verwendung des Fahrrads als sportliches Alltagsrad habe ich hier eine unkomplizierte SRAM-2-Gang-Automatix-Nabe mit Rücktrittfunktion verbaut.

![](/uploads/Pro4_03.jpg)

Ein Anfahrgang schaltet automatisch (geschwindigkeitsabhängig ab ca. 17 km/h) automatisch in einen höheren Gang um. Das funktioniert wunderbar reibungslos. 42/19 hat sich hier als die ideale Übersetzung fürs „Flachland“ erwiesen.

![](/uploads/Pro4_04.jpg)

**Das Ergebnis nach der RAEDERKUR:**

Aus dem etwas in die Jahre gekommenen Renner ist nun ein unkompliziertes, sportliches Alltagsrad geworden. Man merkt dem Rad beim Fahren das sehr gute, solide Ausgangsmaterial an. Die neuen Laufräder sind ein guter Kompromiss zwischen Sportlichkeit und Alltagstauglichkeit. Leicht und agil ist der erste Fahreindruck. Optisch aufgeräumt und auf das Nötigste beschränkt, in farblich edel abgestimmter Harmonie.

![](/uploads/Pro4_01.jpg)

RAEDERKUR - Projekt 04 - Umbau altes Rennrad „Koga Miyata“ - August 2017

Rennrad - Single Speed (2-Gang-Automatik-Schaltung)

**Technische Details:**

| Bauteil | Beschreibung |
| --- | --- |
| Produktbezeichnung | altes Rennrad "KogaMiyata" |
| Umbaukosten | ca. 340 € |
| Rahmen | gemuffter Stahlrahmen aus den 80ern |
| Schaltung | SRAM -2Gang Automatix |
| Kurbel | Shimano |
| Pedale | Gipiemme/Italia schwarz/silber |
| Kettenblatt | 42 |
| Ritzel hinten | 19 |
| Bremsen | Rennbremssatz Miche Performance / schwarz |
| Gabel | starr |
| Felgen | Exal ZX19 / silbern |
| Reifentyp | Continental Contact Plus |
| Reifenformat | 28 Zoll 32-622 SV |
| Sattel | Brooks / Kernledersattel / schwarz |
| Lenker | Riser-Bar/MTB/coolweiß-25,4mm/LK |
| Vorbau | Schaft 22,2mm / Lklemmung25,4mm    weiß |
| Beleuchtung | keine |
| Sicherheit | Seitenreflektoren Reifen / Klingel / Rücktrittbremse |
| Rahmenhöhe | 60 |
| Gewicht | ca 11 kg |
| Zubehör | ----------- |
