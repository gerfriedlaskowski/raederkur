---
date: 2019-07-25T11:20:05.000+00:00
title: RAEDERKUR - Projekt 06 - Umbau Koga Miyata Traveller Bj. Anfang 80er - Mai
  2019
subtitle: Rennrad 8-Gang-Kettenschaltung
tags: []
previewimage: "/uploads/Pro06__komplett.jpg"

---
**Das Ausgangsmaterial:**

Schon eine Weile lagert bei mir ein vom Vorbesitzer zum Citybike umgebautes Koga Miyata Traveller und wartet auf eine „KUR“. Der sehr solide, filigrane Stahlrahmen soll in meinem Sinne in ein „moderates“ Rennrad umgebaut werden. Als sportliche Anlage bringt das Rad schon einmal den Rahmen und die bei diesen Rädern verbauten, unverwüstlichen Lager mit. Kurbel und Steuersatz lassen sich wie am ersten Tag sehr weich und flüssig bewegen. Die Bremsen (Shimanos 105er Serie aus den 80ern) sind, wie alles an dem Rad, gut behandelt worden und machen einen souveränen Eindruck. Felgen von MAVIC, die schmale Rennbereifung relativ neu, mit leichtem Profil versehen, die Naben, der 8-fach-Zahnkranz, alles bestens! Alle verbauten „City“-Komponenten dürfen in den wohlverdienten Ruhestand gehen.

**Die Arbeitsschritte:**

Nachdem der Rahmen von allen Anbauteilen befreit worden ist (Aufkleber sind nervig! ... erwähnte ich schon mal ;-) Abschleifen und Anlaugen. Ich habe hier keine einzige Roststelle gefunden. Gute Pflege, Herr Doktor! In den ersten zwei Arbeitsgängen bringe ich eine Grundierung auf. Dann (immer wieder spannend) die endgültige Rahmenfarbe. In diesem Fall ein kräftiges, leuchtendes Rot. Nach dem zweiten Sprühdurchgang, und entsprechenden Wartezeiten, leuchtet und glänzt der Lack, alles ohne Läufer.

![](/uploads/Pro06__Rahmen-Lenker-sattel.jpg)

**Die Neuteile/Anbauteile:**

In der Zwischenzeit habe ich alle alten Anbauteile gereinigt, neu geölt, poliert. Dank des guten Ausgangmaterials sehen alle Teile nach der Aufbereitung sehr gut aus. Der Renner soll vom Charakter her einerseits seine „Wurzeln“ nicht verbergen und trotzdem frisch, leicht und agil erscheinen.

![](/uploads/Pro06__Friktionsschalhebel.jpg)

Einen gebrauchten, konischen Rennlenker in schwarz-moderner Machart kombiniere ich mit einem ebenfalls schwarzen CONTEC-Vorbau. ![](/uploads/Pro06__Tarantula.jpg)(Modell „TARANTULA“, übrigens das Einzige das ich gefunden habe mit 22,2 mm Schaftklemmung und einer 31,8 mm Lenkerklemmung für die Aufnahme des konischen Lenkers.) Das spiegelt sozusagen die zeitgemäße Komponente am Rad wider. „Retro“ sind die 105er Bremsen samt Bremsgriffen, ein Friktionsschalthebel am Unterrohr, der schöne „Turbo“-Sattel von Selle Italia und die Shimano Tretkurbel mit einem 42er IG-Kettenblatt aus der technischen IG-Aera von Shimano (InteractiveGlide), dem Vorgänger des HG. Bei der Tretkurbel verzichte ich auf zwei der drei Kettenblätter, denn ich möchte lediglich das 42er benutzen. Das Deore-XT-Schaltwerk arbeitet sehr zuverlässig, schnell und leise. Ich verbaue noch eine neue Shimano IG-Kette für 8-fach-Kassetten. Außerdem werden ebenfalls alle benötigten Züge und Außenhüllen durch Neuteile ersetzt.

**Das Ergebnis nach der RAEDERKUR:**

![](/uploads/Pro_06-komplett_001-1.jpg)  
![](/uploads/Pro_06-Schalthebel__005-1.jpg)  
![](/uploads/Pro_06-Gabel__004-1.jpg)  
![](/uploads/Pro_06-Sattelstange-Turbo__006-1.jpg)  
![](/uploads/Pro06__Reifen-105er-hinten.jpg)  
![](/uploads/Pro06__tretkurbel.jpg)

In meinen Augen ein gelungener Versuch, das ehrwürdige Original respektvoll und behutsam aufzufrischen, und die sportlichen Gene dabei in angemessener Art und Weise etwas mehr hervorzuheben. Alle Chromteile, die schwarzen Bauteile und der rote Rahmen harmonieren farblich ausgezeichnet. Das ergibt insgesamt ein aufgeräumtes, zusammenhängendes Bild.

![](/uploads/Pro06__komplett.jpg)  
Das Reduzieren auf die notwendigsten Teile passt zum feinen Stahlrahmen, die sportliche Ausrichtung ist unübersehbar und – vor allem – auch sehr gut fahrbar. Nicht ganz so agil wie ein reinrassiger Rennrahmen; etwas komfortabler und gemäßigter dank der hier vorliegenden Rahmengeometrie des „Traveller“-Modells.

**(Nachtrag - September 2019 )**

\- im Alltagsbetrieb bin ich mit der Kombination altes Kettenblatt /neue Shimanokette für 8-fach-Kassette nicht zufrieden. Unschöne Geräusche machen sich bemerkbar und stören das Fahrvergnügen. Ein Versuch mit einer neuen, schönen MIGHTY-Kurbel mit fest vernietetem 38er Kettenblatt scheitern dummerweise an einer unpassenden „Einbautiefe“ der Kurbel - das Ritzel würde am Rahmen schleifen. Somit noch einmal in die Ersatzteilkiste gegriffen und eine andere Kurbel von Shimano eingebaut. Das ist allerdings eine 3fach-Kurbel.

Da auch hier das kleinste Ritzel verdächtig nahe am Rahmen anliegt löse ich es einfach aus dem Verbund heraus und gewinne so den nötigen Raum. Da ich eh nur das 38er Ritzel benutzen möchte, passt das jetzt gut. Die Einzelbestandteile des Antriebs arbeiten nun wunderbar, nahezu geräuschlos zusammen .

Mitlerweile tendiere ich bei diesem Umbau, in der Funktion, auch eher zum Citybike. Das heißt im Einzelnen; Ich montiere noch einen Satz schöner, glänzend schwarz lackierter Stahlschutzbleche ; einen geraden, schwarzen MTB-Lenker; einen Hinterbau-Seitenständer - das alles zusammen sorgt nun für eine erweiterte, gute Alltagstauglichkeit.

Die Optik des Fahrrads hat sich nun doch sehr verwandelt, wobei allerdings die sportliche Erscheinung nicht unbedingt gelitten hat. Die durch den geraden Lenker etwas aufrechtere Sitzposition  hat für ein „ Alltagsrad“ eindeutige Vorteile, was das Handling und die Übersicht im Strassenverkehr angeht.

![](/uploads/Umbau-PRO-06.jpg)

Die moderaten Umbauten zugunsten der Alltagstauglichkeit, haben das Rad nicht zum Nachteil verändert. Es ist jetzt zu einem sehr agilen, schnörkellosen Stadtrad geworden.

RAEDERKUR - Projekt 06 - Umbau Koga Miyata Traveller Bj. Anfang 80er - Mai 2019

Rennrad 8-Gang-Kettenschaltung

**Technische Details:**

| Bauteil | Beschreibung |
| --- | --- |
| Produktbezeichnung | Rennrad 8-Gang-Kettenschaltung |
| Umbaukosten | ca.180,- € |
| Rahmen | Stahlrahmen Koga Miyata Bj. Anfang 80er |
| Schaltung | Kettenschaltung 8-Gang ( Friktion ) |
| Kurbel | Shimano 2-fach / 38 Zähne / schwarz |
| Pedale | XLC MTB-Pedale PD-M01 silber/schwarz |
| Kettenblatt | 38 |
| Ritzel hinten | Shimano 8-fach Kasette |
| Bremsen | Shimano 105 |
| Gabel | starr |
| Felgen | MAVIC / silber / Steckachse |
| Reifentyp | Continental Contact |
| Reifenformat | 24-622 / 28“ |
| Sattel | Selle Italia Turbo schwarz |
| Lenker | gerader MTB-Lenker Alu schwarz |
| Vorbau | Contec „ Tarantula“ Schaftklemmung 22,2 - Lklemm.31,8 |
| Beleuchtung | optional |
| Sicherheit | Reifen reflektierend |
| Rahmenhöhe | 58 |
| Gewicht | ca.10 kg |
| Zubehör | ---------- |
| Kette | SRAM PC830 / 114/ nickel |
| Rahmenfarbe | rot |
