---
date: 2019-08-12T11:01:38.000+00:00
title: 'RAEDERKUR - Projekt 07 - Neuaufbau Rabeneick Damenrad '
subtitle: Citybike für Damen   -   mixte Rahmen
tags: []
previewimage: "/uploads/PRO7_total_02.jpg"

---
**Das Ausgangsmaterial:**

Schon lange ungenutzt – zuletzt vor ca. 5 Jahren gewartet – Baujahr ca. 1990 – Pentasport 5-Gang-Schaltung mit zwei Schaltzügen – ein echter „mixte“ Stahlrahmen  
(zwei dünne Rohre verlaufen vom Steuerrohr bis zum Ausfallende)

![](/uploads/Raben--03.jpg)

  
![](/uploads/Proj07_001.jpg)

...Felgenbremse vorne – Rücktrittnabe; das sind die Eckdaten dieses qualitativ hochwertigen Damenfahrrads. Es ist viel „rumgekommen“ und hat einiges mitgemacht und dann vor ca. 10 Jahren seinen aktiven Dienst eingestellt und nur noch gelegentlich als Ersatzrad gedient. Auch hier erweisen sich aber die Grundelemente von hoher Qualität.

![](/uploads/Raben--02.jpg)  
Es ist ein neuer Einsatz als Citybike vorgesehen; somit wird das Fahrrad also eine **„Raederkur“** durchlaufen. Es soll im Prinzip optisch und technisch auf einen attraktiven, neuwertigen Zustand gebracht werden, sodass man es wieder gerne benutzen mag.

**Die Arbeitsschritte:**

Wie üblich baue ich zuerst einmal alle Anbauteile auseinander, um mir einen Überblick über den Zustand der einzelnen Bauteile zu verschaffen. Außerdem soll ja der Rahmen auch optisch aufbereitet werden. Auch hier bemerke ich wieder gerne: eine zum Teil ausgezeichnete Qualität der verwendeten Materialien. Alles lässt sich auch nach so vielen Jahren noch recht gut lösen und auseinanderbauen. Als besonders hartnäckig erweist sich allerdings die Tretkurbel. Diese sitzt extrem fest und lässt sich auch mit allen „gewöhnlichen“ Maßnahmen kein bisschen lösen oder lockern. Die Befestigungsschrauben sind leider doch etwas dem Rost verfallen. Extremer Härtefall, extreme Maßnahme: jetzt kommt tatsächlich brachial die Eisensäge zum Einsatz.

![](/uploads/Pro7-gammelige-Kurbel-weg_001.jpg)

Damit trenne ich das Material der Tretkurbel knapp am Vierkant des Tretlagers auf und schließlich „ergibt“ sich die Kurbel.

Der Rahmen hat dem ersten Anschein nach, bis auf wenige Flugroststellen, auch keine nennenswerten Schäden und wird sich sicher, mit etwas Arbeit und einer neuen Lackierung, wieder sehr ansehnlich herrichten lassen.  
Los geht's. Zuerst den Rahmen säubern und grob abschleifen. Die Flugroststellen werden nahezu blank geschliffen und anschließend mit einem Rostumwandler behandelt,

![](/uploads/Pro7-Tretlager-Rostumwandler.jpg)  
welcher gleichzeitig als Grundlage für den neuen Lack dient. Entfetten des Rahmens mit Anlauger und dann als erste Schicht wie immer die Grundierung. Nach ausreichender Trocknung wird es nun spannend, denn ich sprühe die neue Farbe auf. Hierbei muss ich leider feststellen, dass der Farbton zwar genau meinen Erwartungen entspricht (ein recht dunkles Violett), die Sprühdosen ihren Inhalt aber leider nur sehr sporadisch und scheinbar ungerne abgeben. Es rotzt und spuckt etwas. Das ist ärgerlich. – (Gemerkt: ich werde dieses Produkt nicht mehr verwenden!) – Am Ende steht aber dann, mit etwas mehr Geduld und Aufwand, doch noch ein recht akzeptables Lackierergebnis.

**Die Neuteile / Anbauteile:**

Das alte Fahrrad soll eine komplette optische und weitgehend auch technische Umwandlung erfahren. Der neue Schürmann Mach 1-Laufradsatz mit silberfarbener Felge und cremefarbenen 37er Conti-Reifen kommt am Hinterrad mit einer Nexus 3-Gangschaltung mit Rücktritt daher.

![](/uploads/PRO7_3Gang.jpg)

Hinten ist ein 20er Ritzel verbaut. Das Verhältnis in Kombination mit der 3-Gangnabe entspricht einer möglichen Entfaltung von 3,0 - 5,7 m und ist ideal für den Einsatz als Citybike und im flachen Gelände.

![](/uploads/PRO7_Sicherungsscheibe.jpg)

Es folgt die neue Kurbel „M-Wave“ von mighty mit fest vernietetem 38er Ritzel.

![](/uploads/PRO7_Kurbel.jpg)

Schaftvorbau und gefederte Sattelstütze übernehme ich aus dem Altbestand.

![](/uploads/fast fertig.jpg)

Ein neuer Lenker, ein farblich in Creme abgestimmter Sattel, Handgriffe in einer Korkoptik und ein sehr stylisch-eleganter Schutzblechsatz markieren optisch schon mal den Weg zum Endergebnis.

![](/uploads/PRO7_Flat-hinten-Detail.jpg)

Ein Problemfall ist noch die ordentliche Verlegung der Schalt- und Bremszüge. Kabelbinder sehen nicht gut aus. Ich probiere hier mal selbstklebende Halterungsplättchen und Kabelclips aus und werde die Haltbarkeit testen. Das muss sich zuerst im Alltag noch als tauglich erweisen, sieht aber schon mal ganz gut aus.

Ein weiteres kleines, technisches Hilfsmittel, das den Alltag erleichtern soll, werde ich an diesem Rad ausprobieren.

![](/uploads/Pro7-Kettenspanner semivertikal Ausfallende.jpg)

Ein Satz asymmetrischer Kettenspanner für semi-vertikale Ausfallenden zum Nachrüsten sitzt mittlerweile an Ort und Stelle. Die Kette lässt sich hiermit wunderbar genau auf die gewünschte Spannung  
bringen. Das gefällt mir gut...

![](/uploads/Pro7_Kettenspanner_001.jpg)  
![](/uploads/Pro7_Kettenspanner_002.jpg)

Als besonderes Ausstattungsmerkmal soll das Rad einen großen Frontgepäckträger bekommen, damit die Einkaufsfahrt (z. B. zum Wochenmarkt) kein Problem darstellt. Fündig geworden bin ich hier beim Hersteller Brave Classic mit einem, im Gegensatz zu den meist üblichen Gepäckträgersystemen, fest am Rahmen verschraubten, sehr schönen Modell.

![](/uploads/Pro7-Gepäckträger.jpg)

Dieses Prinzip ist insofern vorteilhaft, da hier die Last vollends am Rahmen hängt und nicht wie üblich am Lenker.

![](/uploads/PRO7_Lenker-Gepäck_02.jpg)

![](/uploads/PRO7_Lenker-Gepäck_01.jpg)

Die Montage erweist sich allerdings in Kombination mit der alten Weinmann Mittelzugbremse als etwas problematisch. Nach ein Paar Versuchen mit geeigneten Distanzscheiben bei der Positionierung der Bremse sowie einer korrigierten Anbringung des Gepäckträgers ist alles wie es sein soll. Es trägt bzw. bremst wie es soll ;-)

... und das Gesamtergebnis kann sich auch sehen lassen, wie ich finde... und lässt sich gut und gerne fahren, wie ich hoffe.

![](/uploads/PRO7_total_02.jpg)

![](/uploads/PRO7_hinten-neu.jpg)

**Technische Details:**

| Bauteil | Beschreibung |
| --- | --- |
| Produktbezeichnung | Rabeneick Damenrad Baujahr ca.1990 |
| Umbaukosten | ca.350,- € |
| Rahmen | gemuffter Stahlrahmen „mixte“ Rahmen-Bauweise |
| Schaltung | Shimano Nexus SG3C41 3-Gang Nabenschaltung/Rücktritt |
| Kurbel | Mighty  38/170mm/ALU-poliert / Kettenblatt genietet |
| Pedale | XLC  MTB  PD M01   schwarz/silber |
| Kettenblatt | 38 |
| Ritzel hinten | 20 |
| Bremsen | Weinmann Mittelzug-Felgenbremse |
| Gabel | starr/Stahl |
| Felgen | Schürmann Mach 1  19-622 / Vollachse |
| Reifentyp | Continental Fahrradreifen Ride Tour 28 Zoll creme reflex |
| Reifenformat | 37-622 / 28“ |
| Sattel | XLC / Modell Everyday / creme |
| Lenker | ERGOTEC Lenker Toulouse Level 2 silbern |
| Vorbau | winkelverstellbarer Schaftvorbau Klemmung 22,2 - Lklemm.25,4 |
| Beleuchtung | optional |
| Sicherheit | Reifen reflektierend |
| Rahmenhöhe | 52 |
| Gewicht | ca.13 kg |
| Zubehör | "Brave classic" Vordergepäckträger / Contec “Flat Fender“ ALU-Schutzbleche |
| Kette | Nexus CN NX10 Nabenschaltung für 1-fach 114 |
| Rahmenfarbe | dunkelviolett |
