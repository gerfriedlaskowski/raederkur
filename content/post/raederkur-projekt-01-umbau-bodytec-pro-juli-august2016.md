---
title: RAEDERKUR - Projekt 01 - Umbau Bodytec Pro  - Juli/August 2016
subtitle: Beachcruiser - Single Speed
date: 2019-06-11T22:00:00.000+00:00
tags: []
bigimg: []
text: ''
previewimage: "/uploads/nachher.jpg"

---
**Das Ausgangsmaterial:**

Das Spaßrad habe ich erworben als ein rabattiertes Werbegeschenk einer bekannten Frauenzeitschrift. Der schön geschwungene Stahlrahmen war minimalistisch ausgestattet mit einfachster Technik. Ohne Beleuchtung, ohne Gangschaltung, eine einfache Cantileverbremse, Rücktrittbremse. Punkten konnte das Rad mit Zweifarbenlackierung, einer ausgefallenen Rahmengeometrie, Stahlschutzblechen, überbreitem Cruiser-Lenker, hellwandigen Breitreifen, hübscher Systemspeichung und, nicht zu vergessen, einer „traumhaft wohlklingenden“ riesigen Fahrradklingel.

Das Rad ist schwer, die Übersetzung nichts für „jedermann“, die Optik eher bieder, der Rahmen und das Konzept „Beachcruiser“ aber originell, und somit hatte es eine „RAEDERKUR“ verdient!

![](/uploads/vorher.png "Vorher")

**Die Arbeitsschritte:**

Laufräder, Bremse, Lenker, Sattel/Sattelstütze, Pedale und Tretkurbel bleiben. Alles abbauen und den Rahmen fürs Lackieren vorbereiten. Säubern, entfetten, grob schleifen, säubern, fein schleifen, abkleben: fertig fürs Lackieren.  
  
![](/uploads/0648.jpg)  
![](/uploads/0647.jpg)  
![](/uploads/0645.jpg)  
![](/uploads/0646.jpg)  
![](/uploads/0644.jpg)

Lackiert wird hier mit der Spraydose. Zuerst ein Haftgrund (lohnt sich immer zur Verbesserung der Haftungsfähigkeit des eigentlichen Lacks), dann der Hauptlack. Um das Bike und die auffallende Rahmengeometrie zu noch mehr optischer Präsenz zu verhelfen, habe ich hier ein kräftiges Orange ausgewählt. Der zweite Lackier-Arbeitsgang gelingt prächtig; ohne Läufer oder Dreck und Staub. Das hell-leuchtende Orange bringt die schön geschwungene Rahmengeometrie erst so richtig zur Geltung. Jetzt kann der Lack aushärten. Die schön geformten, breiten Schutz-BLECHE (tatsächlich aus Metall!) werden mattschwarz lackiert, als belebender Kontrast zum Orange und den Chromteilen.

Alle vorhandenen Chromteile und die weiter zu verwendenden Anbauteile werden aufpoliert.

Dank der sehr geringen Laufleistung und der sorgfältigen Lagerung des Beachcruiser vor dem Umbau, sehen alle Teile nach der Aufbereitung wieder wie neu aus.

**Die Neuteile/Anbauteile:**

Als „Gimmick“ und zur optischen Aufwertung bekommt das Bike noch eine orangefarbene Kette und ebenfalls orangefarbene Lenkergriffe spendiert.  
  
![](/uploads/0641.jpg)

**Das Ergebnis nach der RÄDERKUR:**

Nach dem Zusammenbau sieht das Rad nun schon wesentlich ansprechender aus. Das satinierte Orange, das dezent-matte Schwarz und die Hochglanz-Chromteile harmonieren einerseits, bilden aber gleichzeitig sehr interessante Kontraste.

Aus dem biederen Cruiser von der Stange ist hier ein attraktiver „Hingucker“ entstanden, der bei einer gemütlichen Cruiserrunde viele Blicke auf sich zieht.

![](/uploads/nachher.jpg)

**Technische Details:**

| Bauteil | Beschreibung |
| --- | --- |
| Produktbezeichnung | Bodytec Pro |
| Preis | ------------- |
| Rahmen | Beach Cruiser |
| Schaltung | ------------- |
| Kurbel | no name / Aluguß |
| Pedale | Alu / Reflektoren |
| Kettenblatt | ? |
| Ritzel hinten | ? |
| Bremsen | no name / vorne Cantileverbremse / hinten Rücktrittbremse |
| Gabel | no name / starr |
| Felgen | Alu matt |
| Reifentyp | Ballonreifen / weißwand /Lauffläche schwarz |
| Reifenformat | 47 /622 |
| Sattel | no name / schwarz |
| Lenker | sehr breiter Cruiserlenker / chrom |
| Vorbau | no name / chrom |
| Beleuchtung | keine |
| Sicherheit | ----------- |
| Rahmenhöhe | 52 |
| Gewicht | ca.14 kg |
| Zubehör | Reflektoren v/h -- super Fahrradklingel |
