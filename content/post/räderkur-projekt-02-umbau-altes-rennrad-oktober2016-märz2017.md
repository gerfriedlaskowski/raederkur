---
date: 2019-07-04T22:00:00.000+00:00
title: RAEDERKUR - Projekt 02 - Umbau altes Rennrad  - Oktober 2016/März 2017 (Update
  Mai2020)
subtitle: Rennrad - Single Speed
tags: []
previewimage: "/uploads/Front.jpg"

---
**Das Ausgangsmaterial:**

Hierbei handelt es sich um ein bereits zuvor schon einmal umgebautes Rennrad aus den 80er-Jahren. Der gemuffte Rahmen aus 512er Reynolds-Chrommolydän-Stahlrohren ist auch über die Jahre kaum gealtert und in einem ausgezeichneten Zustand. Leichte Rostansätze sind nur oberflächig. Der Rahmen hat eine minimale „Beule“ im Oberrohr. Da mir die Vorbesitzer aber bekannt sind, kann ich von einer sonst sehr pfleglichen Behandlung ausgehen. Einige Anbauteile (Bremsen, Sattelstütze, Tretlager, Steuersatz und Tretkurbel werde ich weiterverwenden, da diese passend zum Rahmen von langlebiger, hochwertiger Qualität sind.

**Die Arbeitsschritte:**

Fahrrad auseinanderbauen und die weiterzuverwendenden Teile reinigen und generalüberholen. Das wären zum einen, neben den Lagern, die 600er Shimano Felgenbremsen inkl. der kombinierten Schalt-Bremsgriffe und der 3T Vorbau. Nun den Rahmen fürs Lackieren vorbereiten. Säubern, entfetten, grob schleifen, säubern, fein schleifen, abkleben: fertig fürs Lackieren. Auch hier kommt zuerst ein Haftgrund auf den vorbereiteten Rahmen. Nach der entsprechenden Trockenzeit darf ich den Hauptlack aufsprühen. Ich habe mich für einen anthrazitfarbenen Lack mit „Hammerschlageffekt“ entschieden. Das Ergebnis erfüllt die Erwartungen.

![](/uploads/Seite-hinten-2.jpg)

Der Lack hält gut, glänzend im Finish, dezent in der Farbgebung. Nur der gewünschte „Hammerschlageffekt“ ist nicht ganz überzeugend... – Nun ja, ich übe noch ;-)

**Die Neuteile/Anbauteile:**

Zum Einsatz kommt hier ein fertig eingespeichter Laufradsatz mit Fixie- bzw. Freilaufritzel (je nach Laufrichtung unterschiedlich verwendbar: fixed = 16 Zähne / Freilauf = 18 Zähne). Ich habe wegen der Optik jeweils eine sehr stabile 40 mm hohe Felge in Schwarz mit gedichteten Industrielagern und radialer Speichung beim Vorderrad ausgesucht. Die Felgen sind CNC-bearbeitet, für besseren Rundlauf, und für die Verwendung von Felgenbremsen geeignet.

Aufgezogen habe ich dann neue Reifen: 32-622/28“ Schwalbe Energizer Plus mit ausgezeichnetem Pannenschutz und sehr guten Allroundfähigkeiten im täglichen Straßenverkehr.

![](/uploads/vorab-Seite-Reflektoren.jpg)

Die alten Pedale tausche ich, auch wegen der Optik, gegen neue, rote Alu-MTB-Pedale aus. Zusammen mit der roten Kette von KMC, und den Lenkergriffen, auch mit roten Applikationen...

![](/uploads/Front-1.jpg)  
...ergibt das ein homogenes, interessantes Erscheinungsbild, wie ich finde. Bei den alten Bremsgriffen entferne ich alle für die Schaltung nötigen Anbauteile. Es bleiben also nur noch der reine Bremsgriffkörper und die dafür notwendige Mechanik übrig. Alle Bremszüge (innen und außen) kommen neu. Die aufpolierte 600er von Shimano fügt sich optisch super ein und hat, mit neuen Bremsgummis bestückt, sehr zuverlässige Verzögerungswerte zu bieten.

Auf dem Gebrauchtmarkt kann ich einen schwarzen MTB-Lenker und einen weißen Rennsattel der Marke XLC pro erwerben. Der Sattel passt optimal zur sehr schlanken Optik des Rades.

![](/uploads/XLC-Pro-Sattel.jpg)

Die Tretkurbel habe ich von 2 auf 1 Ritzel/42 Zähne reduziert. Der alte Rahmen lässt sogar den Anbau von sehr schmalen, schwarzen Kunststoffschutzblechen zu, welche erfreulicherweise die Optik nicht unbedingt stören, im Gegenzug aber ein hohes Maß an Alltagstauglichkeit bieten.

**Das Ergebnis nach der RAEDERKUR:**

![](/uploads/Seite-1.jpg)

Ein sportlicher „Oldtimer“, als Single-Speed aufgebaut, in filigraner Optik, aber mit wuchtigen Rädern. Die Übersetzung 42/18 macht das Anfahren nicht zur Qual und lässt ein ausreichend hohes Tempo für die Bedürfnisse in der Stadt zu. Ein schönes, individuelles Allroundrad mit nostalgischem, sportlichem, alltagstauglichem Flair.

**UPDATE Mai 2020:**

![](/uploads/pro02-gelb_schwarz-update.jpg)

Aus mangelnder Erfahrung bei den esrten Projekten war hier offensichtlich die Lackierung bzw. die Vorarbeitung hierzu nicht sorgfältig genug von mir ausgeführt worden. Die Folge: hier und da zeigte sich Rostbefall am Rahmen. Zeit, Lust und der Spaß an Veränderungen hat dann dazu geführt, das ich das Rad noch einmal komplett zerlegt, grundgereinigt und den Rahmen dann erneut bearbeitet und lackiert habe. Zudem schützt jetzt eine zusätzliche Klarlackschicht vor leichten Beschädigungen und Kratzern.

Hier das Ergebniss:

![](/uploads/pro2-update_komplett.jpg)![](/uploads/pro2-update_detail-hinten-links.jpg)![](/uploads/pro2-update_detail-hinten-rechts.jpg)![](/uploads/pro2-update_600er-bremse.jpg)

Die Sache läuft....

| Bauteil | Beschreibung |
| --- | --- |
| Produktbezeichnung | Rennrad - Single Speed |
| Preis | ca.280,- € |
| Rahmen | gemuffter Stahlrahmen |
| Schaltung | ------------- |
| Kurbel | Shimano |
| Pedale | XLC  MTB Ultralight  silber/rot |
| Kettenblatt | 42 |
| Ritzel hinten | 16 Fix / 18 Freilauf |
| Bremsen | Felgenbremsen Shimano 600 |
| Gabel | starr |
| Felgen | KHE 700c |
| Reifentyp | SchwalbeEnergizer Plus |
| Reifenformat | 32-622/28“ |
| Sattel | XLC_PRO-weiß |
| Lenker | gerade / MTB / schwarz |
| Vorbau | 3 T |
| Beleuchtung | keine |
| Sicherheit | Reifenflanken reflektierend |
| Rahmenhöhe | 56 |
| Gewicht | ca. 11 kg |
| Zubehör | Schutzbleche / Klingel / Seitenständer |
| Kette | KMC rot 1/2" x 1/8" |
| Rahmenfarbe | antrazith Hammerschlag-Effekt |
