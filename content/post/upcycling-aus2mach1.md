---
date: 2020-05-25T04:52:37+00:00
title: 'RAEDERKUR - Projekt 08 - Upcycling - aus2mach1 - '
subtitle: '"Wanderer" - Damenrahmen mit Gebrauchtteilen wiederbelebt'
tags: []
previewimage: "/uploads/pro08_vorab_zusammengebaut.jpg"

---
![](/uploads/pro08_vorab_zusammengebaut.jpg)  
**Das Ausgangsmaterial:**

Der Zufall spielt mir einen sehr schönen Damenrahmen des Herstellers „ WANDERER“ zu. Trotz intensiver Recherchen kann ich leider keine Aussage über eine genaue Modellbezeichnung machen. ( Der „Sporttourer“ mit der Modellbezeichnung  S600 aus dem Jahr 2013 ist zwar ähnlich, aber eben auch nur das ; wenn jemand hier mehr dazu sagen kann, bitte gerne ... ) Auf jeden Fall ist der Rahmen in einem guten, gebrauchten Zustand. Ein Stahlrahmen, Steuersatz und Tretlager sind vorhanden und laufen auf den ersten Blick sauber. Als Idee schwebt mir bei dieses Mal ein „Upcycling“-Projekt vor. Kann es gelingen, unter der Verwendung unterschiedlicher, vorhandener Gebrauchtteile und möglichst weniger Neuteile ein trotzdem optisch und technisch attraktives Fahrrad zusammenzustellen ? Hierzu veranschlage ich mal, als ehrgeiziges Ziel, mit höchsten 50,-€ Neuinvestition und den vorhandenen Ersatzteilen ein ansprechendes, fahrtüchtiges Fahrrad aufzubauen.

![](/uploads/wanderer-rahmen-an-wand.jpg)

![](/uploads/pro08_tretlager.jpg)

Als „Ausschlachtobjekt“ soll mein altes „Arbeitsrad“ von der Firma  „Prophete“ dienen. Dieses habe ich vor ca. 8 Jahren in dem  ortsansässigen Werksverkauf der Fahrradmanufaktur als „Ausstellungsrad“ oder „Modellrad“ erworben. Da haben wir unter Anderem  hochwertige Alu-Hohlkammerfelgen von Rigida und Shimano „ Rollerbrakes“, eine 7-Gang Shimano Nexus Nabenschaltung, Tretkurbel und Pedalarme, Schutzbleche etc.

Ich bin sehr gespannt, was davon in Ergäzung aus meinem weiteren Ersatzteillager, zur Verwendung kommen wird.

**Die Arbeitsschritte:**

Der vorliegende Rahmen wird in diesem speziellen Fall nicht komplett lackiert. Das schön leuchtende Dunkelblau der Originallackierung ist an den meißten Stellen noch sehr gut erhalten. Lediglich die Gabel und einige, kleine Stellen im vorderen Bereich des Rahmens benötigen etwas Korrektur und Pflege.

![](/uploads/pro08_gabel-entrosten.jpg)

Hier ist besonders die Gabel betroffen und wird von mir vom groben Rost befreit und dann die Stellen mit Rostumwandler bearbeitet. Um auch hier Kosten einzusparen, lackiere ich die betroffenen Teile der Gabel mit dem restlichen Lack (s.Update Projekt 2) aus einem anderen Projekt. ( ... mal schauen ob das so bleibt ! )

**Die Neuteile / Anbauteile :**

Jetzt wird es spannend. Zuerst versorge ich den Rahmen mit den zukünftigen Laufrädern.  
![](/uploads/pro08_nur-rad-vorne.jpg)

In diesem Fall sind das 2 Rigida 28“ Alu DP2000 inkl. der verbauten Shimano Rollerbrakes. Die Größe ist passend (auch für den kleinen Rahmen ), nur die Halterungen für die Bremsanlage müssen angepasst werden. Vorne erledigt das ab jetzt eine Rahmenschelle und hinten,

![](/uploads/pro08_befestigung_rollerbrake_hinterrad.jpg)

wie auch schon am Ausgangsrad, eine entsprechend angepasste Schlauchschelle. Schutzbleche vorne und hinten habe ich passende rausgesucht und montiert. Lenker und Vorbau aus dem vorhandenen Fundus, sind auch kein Problem.

![](/uploads/pro08_lenker.jpg)

Die Sattelstütze und Sattelstützenklemme sind aufgrund einer „ungewöhnlichen“ Vermaßung des Stahlrahmens aber etwas problematisch. Hier hilft ein ortsansässiger Fahrradhändler mit viel Geduld und Engagement. ( ... vielen Dank nochmal dafür ! )  
Der Rohrdurchmesser wird mit einer Reduzierhülse auf das geforderte Maß von 30,0mm gebracht um die vorhandene 27,2mm Sattelstütze passend aufzunehmen. Die benötigte Sattelstützenklemme im Format 34,0mm hat der Händler auch vorrätig. Dazu kommen , ein neuer Seitenständer und feine, schraubbare Lenkergriffe für Drehgriffschaltung.(übrigens: mit diesen 4 neuen Teilen und den Kosten für den gebrauchten Rahmen liege ich zwar 2,50 €uro über der angestrebten Max-Renovierungssumme von 50,-€uro  --- das verzeihe ich mir ;-)

Was noch ?  Sattel, Tretkurbel, Kette...sogar die Klingel liegen noch in meinem Ersatzteileregal.

Nachdem alles vollständig angebaut und eingestellt ist, kommt der spannende Augenblick der ersten Probefahrt. Wie wird das Fahrverhalten mit diesen Bauteilen ausfallen ? Welche Fahreigenschaften wird das Rad haben ?  
Eins kann man jetzt schon sagen : optisch fügt sich alles recht gut zusammen und sieht interessant und stiimig aus.

**Das Ergebniss nach der RÄDERKUR :**

![](/uploads/pro08_wanderer-fertig.jpg)

OK. Das ist kein Leichtgewicht geworden, da die vorhandenen Bauteile von einem durchaus stabilen und hochwertigem Trekkingrad kommen. Insgesamt macht das Rad daher aber einen guten und wertigen Eindruck. Die Rollerbremsen lassen sich gut dosieren und verzögern völlig ausreichend fürs Flachland. Laufräder und  Lager drehen erfreulicherweise leise und rund, ebenso der Antriebsstrang. Die schon etwas betagte Nexus 7-Gang Schaltung muss nochmal nachjustiert werden; aber alle Gänge sind „ erreichbar “.  
Das Fahrverhalten ist erwartungsgemäß eher als ausreichend handlich und solide, denn als sportlich und agil zu bezeichnen. Gerade diese Eigenschaften machen das Rad aber für den täglichen Einsatz sehr geeignet.

Somit ist mit wenig finanziellem Neuaufwand, einigen guten Gebrauchtteilen und nur wenigen Neuteilen, ein schönes, sehr alltagstaugliches „Upcycling“-Projekt“ entstanden.

![](/uploads/pro08_vorab_zusammengebaut.jpg)
**Technische Details:**

| Bauteil | Beschreibung |
| --- | --- |
| Produktbezeichnung | Damenrad |
| Umbaukosten | 52,50 € |
| Rahmen | Stahlrahmen WANDERER Bj. ??? |
| Schaltung | Nabenschaltung SRAM Nexus 7-Gang |
| Kurbel | 1-fach / silbern |
| Pedale | Trekking-Pedale /schwarz |
| Kettenblatt | 38 |
| Ritzel hinten | 21 |
| Bremsen | Rollerbrakes Shimano Nexave |
| Gabel | starr |
| Felgen | Regida 28“ DP2000/ Alu-silber / Steckachse |
| Reifentyp | Schwalbe Marathon |
| Reifenformat | 40-622 / 28“ |
| Sattel | Damen City Gel schwarz |
| Lenker | gerader MTB-Lenker Alu schwarz |
| Vorbau | Schaftklemmung |
| Beleuchtung | optional |
| Sicherheit | Reifen reflektierend |
| Rahmenhöhe | 48 |
| Gewicht | ca.14 kg |
| Zubehör | gefederte Sattelstütze |
| Kette | SRAM / nickel |
| Rahmenfarbe | dunkelblau |
