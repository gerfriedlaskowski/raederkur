---
title: RAEDERKUR - Projekt 03     -     Umbau Rennrad „KOTTER“ - Juni 2017
subtitle: Rennrad - Single Speed - 2-Gang-Automatikschaltung
date: 2019-07-08T22:00:00.000+00:00
tags: []
text: ''
previewimage: "/uploads/Projekt03_vorne.jpg"

---
**Das Ausgangsmaterial:**

Ein schönes, in die Jahre gekommenes Rennrad der deutschen Marke Albuch KOTTER, ca. Mitte 80er-Jahre. Sehr schöner perlmuttfarbener, gemuffter Stahlrahmen, hohlraumversiegelt, komplette Shimano 105-Ausstattung, Mavic-Felgen, schmale Rennbereifung. Das Rad wurde lange nicht mehr bewegt. Die nicht mehr ganz zeitgemäße Ausstattung, mit am Rahmen angebrachten Schalthebeln, und eine sehr sportlich ausgelegte Bauweise sollen hier in ein optisch und technisch ansprechendes Rad mit einer gewissen sportlichen Alltagstauglichkeit verändert werden. Hierbei ist es aber erstrebenswert, den Charme der vergangenen Jahre und das „Nostalgische“ des Fahrrads zu erhalten.

![](/uploads/Projekt03_Querrohr-Kotter-racing.jpg)

![](/uploads/Projekt03_FALCK-Rohr.jpg)

**Die Arbeitsschritte:**

Der elegante, filigrane, perlmuttweiße Rahmen mit der geschwungenen Herstelleraufschrift am Querrohr konnte, dank guter Pflege und nur kleiner Lackschäden, mit sämtlichen Lagern übernommen werden. Alle anderen Anbauteile werden abgebaut und ausgetauscht, um den Charakter des Fahrrads mehr in Richtung Alltagstauglichkeit zu bringen. An der Tretkurbel bleibt nur noch ein Kettenblatt (42) übrig.

![](/uploads/Projekt03_Seite-Bauwg.jpg)

**Die Neuteile/Anbauteile:**

Neue Laufräder von Schürmann (ZX19) in silber und die passende Bereifung von Schwalbe in der Größe 32-622/28“ werden verbaut. Um die gewünschte Alltagstauglichkeit mit der sportlichen Ausrichtung zu kombinieren, verwende ich als Antrieb eine Shimano SRAM 2-Gang Automatix-Nabe.

![](/uploads/Projekt03_Sram-2gang-auto.jpg)

Diese schaltet ab einer gewissen Geschwindigkeit automatisch auf den zweiten Gang hoch. Sehr praktisch für die kleine Tour im Flachland, insbesondere natürlich auch durch die jetzt vorhandene Rücktrittbremse. Die beiden 105er Felgenbremsen arbeiten noch sehr zuverlässig. Sie werden aber mit neuen Griffen (AVID Bremshebel Speed Dial 7) verbunden,...

![](/uploads/Projekt03_lenker-Vorbau-AVID-Griffe.jpg)

...da die Originalhebel vom Rennlenker nicht zum neuen, leicht geschwungenen Lenker passen. Die AVID-Hebel hingegen lassen sich sehr gut an die unterschiedlichen Anforderungen anpassen, um bequem auch greifbar zu sein. Neue Bremsleitungen und „griffige“ neue Bremsbeläge runden das Thema Bremsen ab. Ein geschwungener Lenker für die gewünschte, aufrechte Sitzposition mit neuen Lenkergriffen wird dann montiert. Desweiteren wird der alte Rennsattel gegen einen etwas bequemeren, tourentauglichen Gelsattel getauscht.

![](/uploads/Sattel.jpg)

Als kleines „Gimmick“, und damit die Verkehrstauglichkeit gewährleistet ist, montiere ich noch diese stylische Klingel. Sehr unauffällig. Sehr chic. Sehr laut.

![](/uploads/Projekt03_Klingel.jpg)

**Das Ergebnis nach der RAEDERKUR:**

![](/uploads/Projekt03_Seite-Malwand-01.jpg)  
![](/uploads/Projekt03_Seite-Malwand-03.jpg)

Schlank, sehr schick und aufgeräumt kommt dieses Rad daher. Die nagelneuen Anbauteile harmonieren schön mit dem alten Rahmen. Chrom, Schwarz und Perlmuttlack passen ausgezeichnet zusammen. Der Single-Speed-Aufbau macht das Rad extrem wartungsarm, und es macht Freude, wie sich das leichte Gefährt dank der „sportlichen Gene“ und der besonderen Rahmengeometrie bewegen lässt.

Die nun aufrechte, aber dennoch sportliche Sitzposition ermöglicht entspannte, ermüdungsfreie Fahrten, nicht zuletzt durch die 32er Schwalbe Marathon Plus mit Pannenschutz. Sie sind ein sehr guter Kompromiss zwischen Sport- und Alltagsgebrauch. Nach ein wenig Eingewöhnungszeit kommt man mit der 2-Gang-Automatix auch sehr gut zurecht. Sie schaltet zuverlässig und leicht, ohne zu ruckeln. Wer nicht rasen sondern „radeln“ möchte, kommt mit der hier gewählten Übersetzung von 42/21 gut aus.

![](/uploads/Projekt03_Seite-Malwand-02.jpg)

**Technische Details:**

| Bauteil | Beschreibung |
| --- | --- |
| Produktbezeichnung | Rennrad - Single Speed |
| Umbaukosten | ca. 360,- € |
| Rahmen | gemuffter Stahlrahmen |
| Schaltung | SRAM 2-Gang-Automatix mit Rücktritt |
| Kurbel | Shimano |
| Pedale | XLC City Comfort Pedalen |
| Kettenblatt | 42 |
| Ritzel hinten | 21 |
| Bremsen | Felgenbremsen Shimano 105 /  AVIDBremshebelSpeed Dial 7 |
| Gabel | starr |
| Felgen | Schürmann ZX19 silbern |
| Reifentyp | Schwalbe Marathon Plus Pannenschutz 7 |
| Reifenformat | 32-622/28“ |
| Sattel | Terry Ateria Damen max |
| Lenker | leicht geschwungen / Alu |
| Vorbau | Cinelli |
| Beleuchtung | keine |
| Sicherheit | Reifenflanken reflektierend |
| Rahmenhöhe | 52 |
| Gewicht | ca. 10 kg |
| Zubehör |  Klingel  |
| Kette | SRAM / nickel |
| Rahmenfarbe | Perlmuttweiß |