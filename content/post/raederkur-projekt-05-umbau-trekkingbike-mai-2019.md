---
date: 2019-07-24T10:42:37.000+00:00
title: RAEDERKUR - Projekt 05 - Umbau Trekkingbike - Mai 2019
subtitle: Fitnessbike mit 8-Gang-Nabenschaltung
tags: []
previewimage: "/uploads/Pro5-komplett.jpg"

---
**Das Ausgangsmaterial:**

Einem gut 11 Jahre alten Trekkingbike der Firma WINORA mit „guten Anlagen“ soll zu einem sportlich-zeitgemäßem „zweiten Leben“ verholfen werden. Ein cooler, dezenter Look mit weitmöglichst integrierter Technik und aufgeräumtem, modernem Erscheinungsbild. Der Alurahmen, die einstellbare Federgabel, Tretlager, Steuersatz sowie Sattelstütze/Sattel und Vorbau/Lenker-Kombination sind auf einem sehr guten Stand. Schaltung, Laufräder und ein anderes Bremssystem sind vom Besitzer gewünscht. Der Lack ist auch „ab“ und soll erneuert werden.

**Die Arbeitsschritte:**

Strip the Bike! Nur noch die Lager bleiben wo sie sind. (Aufkleber sind total nervig). Schleifen, Anlaugen, erste Grundierung. Das geht sehr gut; auch die Außentemperaturen spielen da mit. Zweite Grundierung und dann schwarzmatt lackieren. Auch hier zwischen den Arbeitsgängen immer schön trocknen lassen. Nach der zweiten Schicht Farbe nun noch der 2K-Klarlack. (Wenn der richtig ausgehärtet ist, soll das die Unempfindlichkeit enorm erhöhen). Ok. Der Rahmen ist fertig. Jetzt wird zusammengebaut.

**Die Neuteile / Anbauteile:**

Die mit den neuen 32er Schwalbe-Marathon-PLUS-Reifen bestückten schwarzen Mavic A319-Felgen sehen mit den jeweils schwarzen Naben sehr elegant aus. Als Antrieb kommt hier eine 8-Gang Shimano Alfine zum Einsatz. Aufgrund der dem Rahmen eigenen, vertikalen Ausfallenden benötigt der Antriebstrakt leider einen Kettenspanner: Alfine, passend in schwarz (stört aber zum Glück dann doch nicht das Gesamtbild!)  
  
![](/uploads/Pro5-Kettenspanner.jpg)

Nun zu den Bremsen. Es sollte ein möglichst wartungsarmes aber „potentes“ System werden. Hier scheinen die Magura HS33 genau das Richtige zu sein.  
  
![](/uploads/Pro5-HS33.jpg)  
![](/uploads/Pro5-vorne.jpg)

![](/uploads/Pro5-Bremsgriff.jpg)  
Das Kürzen der Leitungen geht (wider meine Erwartung) ganz problemlos vonstatten. Überhaupt ist das Montieren und Einstellen sehr „userfreundlich“ und unkompliziert.

**Das Ergebnis nach der RAEDERKUR:**

Der Aufwand hat sich gelohnt. „Neu“ und „Alt“ fügen sich jetzt zu einem homogenen, optischen Ganzen zusammen. Mattes, seidenglänzendes Schwarz, grau und wenige Chromteile erzeugen einen elegant-dezenten Auftritt mit eindeutig sportlichen Tendenzen. Die 32er Allroundbereifung ist gut alltagstauglich (integrierter Pannenschutz) und lässt trotzdem eine schnelle Gangart zu. Die Übersetzung der Schaltung sollte evtl. nochmal bei Zeiten, über den Austausch des hinteren 20er-Ritzels gegen ein 19er bzw. 18er, zur effektiveren Entfaltung geändert werden. Ansonsten ist auch hier die „Reduzierung/Vereinfachung“ sehr gelungen und willkommen. Der positive Eindruck beim Einbau der Bremsen wird durch den Einsatz im Alltag auf jeden Fall bestätigt. Zuverlässig und sicher.

Nicht zuletzt durch die coole Art der in die Sattelstütze und den Lenker integrierten Beleuchtung...  
  
![](/uploads/Pro5-vorne.jpg)![](/uploads/Pro5-LightSkin.jpg) ...und die verbaute, restliche hochwertige Ausstattung, steht hier jetzt ein sehr individuelles Bike am Ende der Arbeiten, das hoffentlich lange Spaß machen wird.  
  
![](/uploads/Pro5-komplett.jpg)

RAEDERKUR - Projekt 05 - Umbau Trekkingbike - Mai 2019

sportliches Fitnessbike mit 8-Gang-Nabenschaltung

**technische Details:**

| Bauteil | Beschreibung |
| --- | --- |
| Produktbezeichnung | sportliches Fitnessbike mit 8-Gang-Nabenschaltung |
| Umbaukosten | ca. 530,- € |
| Rahmen | Winora Alurahmen geslopt / Crossbike |
| Schaltung | Shimano Alfine 8-Gang-Nabenschaltung / schwarz / Freilauf |
| Kurbel | Shimano / FC C6000 / 38 Zähne / single / schwarz |
| Pedale | MTB |
| Kettenblatt | 38 |
| Ritzel hinten | 20 |
| Bremsen | hydraulische Felgenbremsen / MAGURA HS33 |
| Gabel | Federgabel / justierbar |
| Felgen | MAVIC A319 / schwarz / Vollachse |
| Reifentyp | Schwalbe Marathon-PLUS / Reflex |
| Reifenformat | 32-622 / 28“ |
| Sattel | Respiro / LightSkin schwarz |
| Lenker | Hikari schwarz |
| Vorbau | Ahead |
| Beleuchtung | LED / integriert in Lenker und Sattelstütze |
| Sicherheit | integrierte Beleuchtung / reflektierende Reifenflanken |
| Rahmenhöhe | 52 |
| Gewicht | ca.11 kg |
| Zubehör |  ----------  |
| Kette | SRAM / nickel |
| Rahmenfarbe | mattschwarz |