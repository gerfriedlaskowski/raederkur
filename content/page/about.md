---
title: Über mich
subtitle: 
comments: false

---
**![](/uploads/Visite-Raederkur.jpg)**

**„... glauben Sie bloß nicht, wen Sie da vor sich haben...“**

Das Schöne entsteht oft durch das „Weniger“. Etwas bewusst auf das Wesentliche oder Nützliche oder Ästhetische zu reduzieren, bzw. eine Kombination aus all diesen Attributen zu erschaffen, steht diesen Projekten und der Beschäftigung mit dem Thema Fahrrad voran. Das Fahrrad ist für mich seit jeher vorwiegend Fortbewegungsmittel Nr. 1 gewesen. Hinzu kamen dann nach und nach andere Funktionen und Aspekte:  
  
•  mein erstes neues Fahrrad für den Schulweg mit 3-Gangschaltung und 2 Handbremsen und Freilauf  
  
•  die erste Fahrt auf einem „echten“ Rennrad

•  ein schön restauriertes, gemütliches „Herrenrad“

•  der solide aber unemotionale „Ackergaul“ für den täglichen Weg zur Arbeit

•  der Erwerb eines edlen, handgefertigten Einzelstücks im Stil eines „Cafe Racers“

•  bis hin zum leichten, wendigen „Single-Speed“

Immer schon habe ich auch Reparaturen und kleine Veränderungen an den Rädern selbst ausgeführt. Aus der Notwendigkeit ist in den vergangenen Jahren eine Leidenschaft geworden. Insbesondere dann, wenn es darum geht, den ideellen und materiellen Wert guter Dinge zu erhalten oder eventuell in ganz neuen Kombinationen wieder zu neuem Glanz zu verhelfen.

Das Interessante daran ist einerseits der **„Schaffensprozess“ --- DIE RAEDERKUR ---**  
(jedes Projekt hat neue technische Herausforderungen zu bieten), dann die Freude am „Entdecken“ des Potenzials von jedem einzelnen Objekt, ein schön geformter Rahmen, super zuverlässige Technik auch nach langen Jahren des Gebrauchs, wie werden die Kombinationen der ausgesuchten Bauteile miteinander wirken? ... am Ende dann im Idealfall ein „individuelles Liebhaberfahrzeug“ mit „Charakter“, **das man gerne fährt ... weil es gut fährt...**

**und zudem auch noch gut aussieht!**

In diesem Sinne versuche ich weiterzumachen und hoffentlich noch vielen „Patienten“  eine wiederbelebende **„Raederkur_"_** zukommen zu lassen.

![](/uploads/logo rund gerfried wasser gefüllt-ohne-Linie.jpg)